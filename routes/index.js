var express = require('express');
var router = express.Router();
var request = require('request');
var path = require('path');
var index = require('../middleware/index/index.js');

router.get('/', (req, res) => {

});
router.get('/index', (req, res) => {
    res.sendFile(path.join(__dirname + '/../views/index.html'));
});

router.get('/validate_user_call',(req, res) => {
    var user = req.query.user;
    var password = req.query.password;
    index.log_in(user, password)
        .then(response => {
            res.json(response);
        })
        .catch(error => {
            console.log(error);
            res.send(error);
        })
});

router.post('/pop', (req, res) => {
    var id = req.body.sku;
    var sucursal = req.body.sucursal;
    res.cookie('sucursal', sucursal)
    res.cookie('sku', id);
    res.send(req.body);
});

module.exports = router;