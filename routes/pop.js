var express = require('express');
var router = express.Router();
var request = require('request');
var path = require('path');
var pop = require('../middleware/pop/pop.js');
var api = require('../middleware/api/index.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/../views/pop.html'));
});

router.get('/look_up', (req, res) => {
    var id = req.query.id;
    pop.look_up(id)
        .then(response => {
            res.json(response);
        })
        .catch(error => {
            console.log(error);
            res.send(error);
        })
});

router.get('/look_up_plans', (req, res) => {
    var id = req.query.id;
    var user = req.query.cookie;
    pop.look_up_plan(id, user)
        .then(response => {
            res.json(response);
        })
        .catch(error => {
            console.log(error);
            res.send(error);
        })
});

router.get('/look_up_price', (req, res) => {
    var sku = req.query.id.toLowerCase().replace(/[^0-9.]/g, "")
    var sucursal = req.query.sucursal;
    pop.look_up_prices(sku, sucursal)
        .then(response => {
            res.send(response)
        })
        .catch(error => {
            console.log(error);
            res.send(error);
        })
});
module.exports = router;