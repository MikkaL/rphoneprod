var jsons = [],
    isArray = !1,
    cellArray = [],
    lastSku = "",
    length, jsonlength = 0,
    idglob = 0;

function printPOST() {
    void 0 == Cookies.get("sku") && window.close();
    cellArray = arraylify(Cookies.get("sku"));
    length = findlength(cellArray);
    lastSku = cellArray[cellArray.length - 1][cellArray[cellArray.length - 1].length - 1];
    isArray = Array.isArray(cellArray[0]);
    for (i in cellArray)
        if (isArray)
            for (e in cellArray[i]) 0 != e && call_data(cellArray[i][e], callback, cellArray[i]);
        else call_data(cellArray[i], callback);
    Cookies.remove("sucursal");
    Cookies.remove("sku")
}

function call_data(a, d, b) {
    $.ajax({
        type: "GET",
        url: "/pop/look_up",
        data: { id: a },
        error: function(a) {
            a = a.responseText.split("<pre>")[1].split("</pre>")[0];
            alert(a)
        },
        success: function(a) { d(a) }
    });
    if (a == search_last_sku(a))
        for (i in b) 0 != i && call_plans(b[i], callback_planes, b)
}

function call_plans(a, d, b) {
    var f = Cookies.get("sessionlog");
    $.ajax({
        type: "GET",
        url: "/pop/look_up_plans",
        data: { id: a, cookie: f },
        error: function(a) {
            a = a.responseText.split("<pre>")[1].split("</pre>")[0];
            alert(a)
        },
        success: function(b) { d(b, a) }
    });
    if (a == search_last_sku(a))
        for (i in b) 0 != i && call_price(b[i], callback_price)
}

function call_price(a, d) {
    var b = Cookies.get("sucursal");
    $.ajax({
        type: "GET",
        url: "/pop/look_up_price",
        data: { id: a, sucursal: b },
        error: function(a) {
            a = a.responseText.split("<pre>")[1].split("</pre>")[0];
            alert(a)
        },
        success: function(b) { d(b, a) }
    })
}

function callback(a) {
    a = JSON.stringify(a);
    a = JSON.parse(a);
    if (void 0 == a.error) {
        var d, b, f, h, g;
        for (i in a.attributes) "mod_procesador" == a.attributes[i].identifier && (d = i), "tipo_procesador" == a.attributes[i].identifier && (d = i), "camara_mp" == a.attributes[i].identifier && (b = i), "pantalla_pulgadas" == a.attributes[i].identifier && (f = i), "color" == a.attributes[i].name.toLowerCase() && (h = i), "operador" == a.attributes[i].identifier && (g = i);
        var k = a.partNumber,
            m = a.name,
            n = "",
            p = "",
            l = "",
            q = "",
            r = "liberado";
        void 0 != d && (n = a.attributes[d].value);
        void 0 != b && (p = a.attributes[b].value);
        void 0 != f && (l = a.attributes[f].value);
        void 0 != g && (r = a.attributes[g].value.toLowerCase());
        if (void 0 != h && (void 0 == a.colors && (q += a.attributes[h].value), void 0 == a.attributes[h].value))
            for (i in a.colors) q += a.color[i];
        d = a.prices.formattedListPrice;
        b = a.prices.formattedOfferPrice;
        f = !1;
        parseInt(a.prices.cardPrice) <= parseInt(a.prices.offerPrice) && (b = a.prices.formattedCardPrice, f = !0);
        isArray && (m = search_name_by_sku(k));
        fillJsons(k, m, n, p, l, q, r, d, b, f);
        k == search_last_sku(k) &&
            (search_position_by_name, document.getElementById("body").innerHTML += fillHTMLCaracteristicas(jsons[i]))
    } else jsons.push("")
}

function callback_planes(a, d) {
    var b = JSON.stringify(a);
    b = JSON.parse(b);
    var f = search_position_by_name(search_name_by_sku(d));
    f = jsons[f];
    if ("" != b)
        for (i in b) {
            var h = b[i].plan_operator.toLowerCase(),
                g = f.id,
                k = '<table style="margin:auto"><tr><td class="plan-detail">Valor Inicial</td></tr><tr><td class="plan-price">$' + formatPrice(b[i].device_price) + '</td></tr><tr><td class="plan-detail">con un plan de $' + formatPrice(b[i].plan_monthly_cost);
            b[i].is_portability && (k += " P*", document.getElementById("disclaimer" +
                g).style.visibility = "visible");
            k += "</td></tr></table>";
            var m = h + "Plan" + g;
            h = h + "Plancard" + g;
            g = "planesbar" + g;
            document.getElementById(g).rowSpan = "5";
            document.getElementById(g).style.visibility = "visible";
            document.getElementById(g).style.background = "#646464";
            document.getElementById(m).innerHTML = k;
            document.getElementById(h).innerHTML = '<img class="img-logo" src="../../public/images/card.svg">'
        }
}

function callback_price(a, d) {
    var b = JSON.stringify(a);
    JSON.parse(b);
    b = search_position_by_name(search_name_by_sku(d));
    b = jsons[b];
    var f = "nada";
    if (0 < b.claro.sku.length)
        for (c in b.claro.sku) b.claro.sku[c].trim() == d && (f = "claro");
    if (0 < b.entel.sku.length)
        for (c in b.entel.sku) b.entel.sku[c].trim() == d && (f = "entel");
    if (0 < b.wom.sku.length)
        for (c in b.wom.sku) b.wom.sku[c].trim() == d && (f = "wom");
    if (0 < b.movistar.sku.length)
        for (c in b.movistar.sku) b.movistar.sku[c].trim() == d && (f = "movistar");
    if (0 < b.liberado.sku.length)
        for (c in b.liberado.sku) b.liberado.sku[c].trim() ==
            d && (f = "liberado");
    var h = f + "PrecioN" + b.id,
        g = f + "PrecioOP" + b.id,
        k = f + "RipleyCard" + b.id,
        m = document.getElementById(h).innerHTML.replace(/[^0-9.]/g, ""),
        n = document.getElementById(g).innerHTML.replace(/[^0-9.]/g, "");
    n = NaN == n ? 0 : n;
    m = NaN == m ? 0 : m;
    if ("nada" != f) {
        0 == m && (document.getElementById(h).innerHTML = "$" + formatPrice(a.precioMaster));
        m > a.precioMaster && (document.getElementById(h).innerHTML = "$" + formatPrice(a.precioMaster));
        for (i in a.promoPrices) f = "S" == a.promoPrices[0].tr ? !0 : !1, h = a.promoPrices[0].precio - a.promoPrices[0].mdesc,
            0 == n && (document.getElementById(g).innerHTML = "$" + formatPrice(h)), n > h && (document.getElementById(g).innerHTML = "$" + formatPrice(h), f ? document.getElementById(k).style.visibility = "visible" : document.getElementById(k).style.visibility = "hidden");
        document.getElementById("container" + b.id).style.visibility = "visible"
    }
    d == lastSku && print()
}

function fillJsons(a, d, b, f, h, g, k, m, n, p) {
    operator = "";
    var l = search_name(d);
    if (null == l) l = '{"id": ' + idglob + ',"nombre" : "' + d.replace(/"/g, '\\"') + '","procesador" : "' + b.replace(/"/g, '\\"') + '","camara" : "' + f.replace(/"/g, '\\"') + '","pantalla" : "' + h.replace(/"/g, '\\"') + '","color" :["' + g.replace(/"/g, '\\"') + '"],"claro": {"sku": [],"precioN": "","precioOP": "","ripleyCard": false},"entel": {"sku": [],"precioN" : "","precioOP" : "","ripleyCard" : false},"movistar": {"sku": [],"precioN" : "","precioOP" : "","ripleyCard" : false},"wom": {"sku": [],"precioN" : "","precioOP" : "","ripleyCard" : false},"liberado": {"sku": [],"precioN" : "","precioOP" : ""}}',
        g = JSON.parse(l), g[k].sku.push(a), g[k].precioOP = n, g[k].ripleyCard = p, jsons.push(g), idglob++;
    else {
        d = !1;
        for (i in l.color) l.color[i].toLowerCase() == g.toLowerCase() && (d = !0);
        d || l.color.push(g);
        l[k].sku.push(a);
        l[k].precioN = m;
        l[k].precioOP = n;
        l[k].ripleyCard = p
    }
}

function fillHTMLCaracteristicas(a) {
    var d = "",
        b = "";
    if (1 < a.color.length) { for (i in a.color) 0 < a.color[i].length && (d += " " + a.color[i] + " |"); "|" == d[d.length - 1] && (d = d.substring(0, d.length - 2)) } else d = a.color[0]; - 1 == a.pantalla.indexOf('"') && 0 < a.pantalla.length && (b = a.pantalla + '"');
    return '<div class="container-pop" id="container' + a.id + '"><div id="popheader" class="box-container sbtitle"><div id="nombre" class="box-title sb2">' + a.nombre + '</div><table class="table-pop"><tr><td class="charac"><div class="box sb1"><p class="caract"> PROCESADOR </p></div></td><td><div class="box-b"><p id="procesador" class="desc">' +
        a.procesador + '</p></div></td></tr><tr><td class="charac"><div class="box sb1"><p class="caract">CAMARAS</p></div></td><td><div class="box-b"><p id="camara" class="desc">' + a.camara + '</p></div></td></tr><tr><td class="charac"><div class="box sb1"><p class="caract">PANTALLA</p></div></td><td><div class="box-b"><p id="pantalla" class="desc">' + b + '</p></div></td></tr><tr><td class="charac"><div class="box sb1"><p class="caract">COLORES</p></div></td><td><div class="box-b"><p id="color" class="desc">' + d +
        '</p></div></td></tr></table></div><div id="popprice" class="pop-content-price"><table class="table-phone"><tr><td class="logos-header"></td><td class="table-pop-header">PRECIO NORMAL PREPAGO</td><td class="table-pop-header">PRECIO OFERTA PREPAGO</td><td class="notif-tarjeta"></td><td id="planesbar' + a.id + '" class="planes-bar" rowspan="1" style="visibility:hidden;"><div class="rotated">PLANES</div></td><td id="valor-plan" class="table-pop-header">VALOR EQUIPO CON PLAN</td><td class="notif-tarjeta"></td></tr><tr style="height: 40px;"><td><img src="../public/images/claro.svg" class="img-logo"/></td><td id="claroPrecioN' +
        a.id + '" class="pop-cell"> </td><td id="claroPrecioOP' + a.id + '" class="pop-cell"> </td><td class="pop-cell"><img id="claroRipleyCard' + a.id + '" style="visibility: hidden" class="img-logo" src="../../public/images/card.svg"></td><td id="claroPlan' + a.id + '" class="pop-cell"></td><td id="claroPlancard' + a.id + '"></td></tr><tr class="row gray"><td><img src="../public/images/entel.svg" class="img-logo"/></td><td id="entelPrecioN' + a.id + '" class="pop-cell"> </td><td id="entelPrecioOP' + a.id + '" class="pop-cell"> </td><td class="pop-cell"><img id="entelRipleyCard' +
        a.id + '" style="visibility: hidden" class="img-logo" src="../../public/images/card.svg"></td><td id="entelPlan' + a.id + '" class="pop-cell"></td><td id="entelPlancard' + a.id + '"></td></tr><tr style="height: 40px;"><td><img src="../public/images/movistar.svg" class="img-logo" /></td><td id="movistarPrecioN' + a.id + '" class="pop-cell"> </td><td id="movistarPrecioOP' + a.id + '" class="pop-cell"> </td><td class="pop-cell"><img id="movistarRipleyCard' + a.id + '" style="visibility: hidden" class="img-logo" src="../../public/images/card.svg"></td><td id="movistarPlan' +
        a.id + '" class="pop-cell"></td><td id="movistarPlancard' + a.id + '"></td></tr><tr class="row gray"><td><img src="../public/images/wom.svg" class="img-logo" /></td><td id="womPrecioN' + a.id + '" class="pop-cell"> </td><td id="womPrecioOP' + a.id + '" class="pop-cell"> </td><td class="pop-cell"><img id="womRipleyCard' + a.id + '" style="visibility: hidden" class="img-logo" src="../../public/images/card.svg"></td><td id="womPlan' + a.id + '" class="pop-cell"></td><td id="womPlancard' + a.id + '"></td></tr><tr style="height: 40px;"><td><img src="../public/images/liberado.svg" class="img-logo" /></td><td id="liberadoPrecioN' +
        a.id + '" class="pop-cell"> </td><td id="liberadoPrecioOP' + a.id + '" class="pop-cell"> </td><td id="liberadoRipleyCard' + a.id + '" class="pop-cell"></td><td></td><td class="pop-cell"></td></tr></table></div><div id="disclaimer' + a.id + '" class="disclaimer" style="visibility: hidden">Contrataci\u00f3n del plan sujeta a evaluaci\u00f3n comercial de cada operador. Valores informados aplican respecto de planes activos durante 12 \u00f3<br/>18 meses, seg\u00fan oferta comercial vigente del operador. Cliente podr\u00e1 tener un m\u00e1ximo de 5 mandatos vigentes en forma simult\u00e1nea.</div></div>'
}

function arraylify(a) {
    var d = a.replace(/\r\n/g, "\n").split("\n");
    a = [];
    for (i in d) 0 < d[i].length && a.push(d[i]);
    d = [];
    var b = [],
        f = [];
    for (i in a) isNaN(a[i].substring(0, a[i].length - 1)) && isNaN(a[i].substring(3, a[i].length)) && f.push(parseInt(i, 10));
    var h = 0;
    if (0 == f.length) return a;
    for (i in a) i == f[h] && (h++, 0 < b.length && (d.push(b), b = [])), b.push(a[i]), i == a.length - 1 && d.push(b);
    return d
}

function search_name(a) {
    for (i in jsons)
        if (a == jsons[i].nombre) return jsons[i];
    return null
}

function search_position_by_name(a) {
    for (i in jsons)
        if (a == jsons[i].nombre) return i;
    return null
}

function look_up_name(a) { return search_name_by_sku(a).name }

function findlength(a) {
    var d = 0;
    for (i in a)
        for (e in a[i]) isNaN(a[i][e].substring(0, a[i][e].length - 1)) && isNaN(a[i][e].substring(3, a[i][e].length)) && d++;
    return d
}

function formatPrice(a) {
    a = parseFloat((a + "").replace(/[^0-9\.]/g, ""));
    if (isNaN(a) || 0 === a) return (0).toFixed(0);
    a = "" + a.toFixed(0);
    a = a.split(".");
    for (var d = /(\d+)(\d{3})/; d.test(a[0]);) a[0] = a[0].replace(d, "$1,$2");
    return a.join(".")
}

function search_name_by_sku(a) {
    for (i in cellArray)
        for (e in cellArray[i])
            if (a == cellArray[i][e]) return cellArray[i][0]
}

function search_last_sku(a) {
    for (i in cellArray)
        for (e in cellArray[i])
            if (a == cellArray[i][e]) return cellArray[i][cellArray[i].length - 1]
}

function search_array(a) {
    for (i in cellArray)
        for (e in cellArray[i])
            if (a == cellArray[i][e]) return cellArray[i]
}

function isSku(a) { return 5 < a.replace(/[^0-9\.]/g, "").length ? !0 : !1 };