$("#fileselect").on("change", function() {
    var a = document.getElementById("fileselect");
    if (/^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/.test(a.value.toLowerCase()))
        if ("undefined" != typeof FileReader) {
            var b = new FileReader;
            b.readAsBinaryString ? (b.onload = function(a) { ProcessExcel(a.target.result) }, b.readAsBinaryString(a.files[0])) : (b.onload = function(a) {
                var c = "";
                a = new Uint8Array(a.target.result);
                for (var b = 0; b < a.byteLength; b++) c += String.fromCharCode(a[b]);
                ProcessExcel(c)
            }, b.readAsArrayBuffer(a.files[0]))
        } else alert("This browser does not support HTML5.");
    else alert("Please upload a valid Excel file.")
});

function checklogin() {
    void 0 != Cookies.get("sessionlog") ? document.getElementById("pop-up").style.visibility = "hidden" : (document.getElementById("pop-up").innerHTML = '<div class="log-in-pop"><div class="tablecontainer"><table><tr><td class="log-in-cell">RUT</td><td><input type="text" name="user" id="rut"/></td></tr><tr><td colspan="2"><p id="userNotice"></p></td></tr><tr><td class="log-in-cell">Contrase&#241;a</td><td><input type="password" name="password" id="password"></td></tr><tr><td colspan="2"><p id="errorNotice"></p></td></tr><tr><td colspan="2"><input type="button" onclick="log_in();" class="button_pop" name="ingresar" value="ingresar"></td></tr></table></div></div>', document.documentElement.style.overflow =
        "hidden")
}
$("#rut").on("change", function() { valida_Rut($(this).val()) || (document.getElementById("userNotice").innerHTML = "Rut Invalido") });

function log_in() {
    var a = formateaRut($("#rut").val()),
        b = $("#password").val();
    valida_Rut(a) && 0 < b.length ? $.ajax({
        type: "GET",
        url: "/validate_user_call",
        data: { user: a, password: b },
        error: function(a) {
            a = a.responseText.split("<pre>")[1].split("</pre>")[0];
            alert(a)
        },
        success: function(a) { callback(a) }
    }) : document.getElementById("errorNotice").innerHTML = "RUT o Contrase&#241;a Inv\u00e1lido"
}

function callback(a) {
    if (void 0 == a.detail) {
        a = formateaRut($("#rut").val());
        var b = $("#password").val();
        Cookies.set("sessionlog", btoa(a + ":" + b));
        checklogin()
    } else document.getElementById("errorNotice").innerHTML = "RUT o Contrase&#241;a Inv\u00e1lido"
}

function ProcessExcel(a) {
    a = XLSX.read(a, { type: "binary" });
    var b = XLSX.utils.sheet_to_row_object_array(a.Sheets[a.SheetNames[0]]);
    a = [];
    for (var c = [], d = 0; d < b.length; d++) void 0 != b[d].NOMBRE && (c[0] != b[d].NOMBRE && 0 < c.length && (a.push(c), c = []), c.push(b[d].NOMBRE)), c.push(b[d].SKU), d + 1 == b.length && a.push(c);
    b = "";
    for (d in a)
        for (e in a[d]) b += a[d][e] + "\n";
    $("#sku").val(b)
}

function create_pop() {
    var a = $("#sucursal").val(),
        b = $("#sku").val();
    $.ajax({
        type: "POST",
        url: "/pop",
        data: { sku: b, sucursal: a },
        error: function(a) {
            a = a.responseText.split("<pre>")[1].split("</pre>")[0];
            alert(a)
        },
        success: function(a) {
            $("#content").html(a);
            window.open("../../views/pop.html")
        }
    })
}

function formateaRut(a) {
    a = a.replace(/^0+/, "");
    if ("" != a && 1 < a.length) {
        a = a.replace(/\./g, "").replace(/-/g, "");
        var b = a.substring(0, a.length - 1),
            c = "",
            d, f = 1;
        for (d = b.length - 1; 0 <= d; d--) c = b.charAt(d) + c, 0 == f % 3 && f <= b.length - 1 && (c = "." + c), f++;
        a = a.substring(a.length - 1);
        c = c + "-" + a
    }
    return c
}

function valida_Rut(a) {
    var b = "",
        c = a;
    if (void 0 == c) return !1;
    if (0 < c.length) {
        crut = a;
        largo = crut.length;
        if (2 > largo) return !1;
        for (i = 0; i < crut.length; i++) " " != crut.charAt(i) && "." != crut.charAt(i) && "-" != crut.charAt(i) && (b += crut.charAt(i));
        crut = b;
        largo = crut.length;
        a = 2 < largo ? crut.substring(0, largo - 1) : crut.charAt(0);
        dv = crut.charAt(largo - 1);
        if (null == a || null == dv) return 0;
        suma = 0;
        mul = 2;
        for (i = a.length - 1; 0 <= i; i--) suma += a.charAt(i) * mul, 7 == mul ? mul = 2 : mul++;
        res = suma % 11;
        1 == res ? a = "k" : 0 == res ? a = "0" : (dvi = 11 - res, a = dvi + "");
        return a !=
            dv.toLowerCase() ? !1 : !0
    }
};