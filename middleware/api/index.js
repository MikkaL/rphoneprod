const request = require('request');

const {
  simpleEndpoint,
  stagingEndpoint,
  planesEndpoint,
  productosEndpoint,
  mpromoEndpoint,
  appVersion,
  ripleyVendedor,
  ripleyPos,
  ripleyApiKey,
  ripleyApiKeyProd,
  ripleyApiKeyPromo
} = process.env;

function make_API_call(sku) {
    const options = {
        method: 'GET',
        uri: simpleEndpoint + sku,
        json: true
    };
    return new Promise(resolve => {
        request(options, function(error, response, body) {
            var stringified = JSON.stringify(body);
            var parsed = JSON.parse(stringified);
            resolve(body);
        }, function(error, response, body) {
            if (!error)
                resolve(body);
        });
    });
}

function validate_user_call(user, password) {
    const options = {
        method: 'GET',
        uri: stagingEndpoint,
        json: true,
        headers: {
            Authorization: 'Basic ' + Buffer.from(user + ':' + password).toString('base64')
        }
    };
    return new Promise(resolve => {
        request(options, function(error, response, body) {
            var stringified = JSON.stringify(body);
            var parsed = JSON.parse(stringified);
            resolve(body);
        }, function(error, response, body) {
            if (!error)
                resolve(body);
        });
    });
}

function make_API_call_productos(sku, sucursal) {
    var output = sku.toLowerCase().replace(/[^0-9.]/g, "");
    var suc = sucursal;
    const options = {
        method: 'get',
        url: productosEndpoint + sucursal + '?ean13=0'+output,
        json: true,
        headers: {
            Accept: 'application/json',
            'RIPLEY-APP-VERSION': appVersion,
            'RIPLEY-SUCURSAL': '00' + suc,
            'RIPLEY-VENDEDOR': ripleyVendedor,
            'RIPLEY-POS': ripleyPos,
            'Ocp-Apim-Subscription-Key': ripleyApiKeyProd
        },
        data: {
            ean13: 0 + output
        }
    };
    return new Promise(resolve => {
        request(options, function(error, response, body) {
            resolve(body);
        }, function(error, response, body) {
            if (!error)
                resolve(body);
        });
    });
}

function make_API_call_MPromo(xml) {
    var length = 0;
    if (xml.length > 1000) {
        length = '0'+xml.length;
    }
    else{
        length = '00'+xml.length;
    }
    var body = length+xml;
    const options = {
        method: 'post',
        url: mpromoEndpoint,
        headers: {
            'Content-Type': 'text/xml',
            'Ocp-Apim-Subscription-Key': ripleyApiKeyPromo
        },
        body: length+xml
    };
    return new Promise(resolve => {
        request(options, function(error, response, body) {
            resolve(body);
        }, function(error, response, body) {
            if (!error)
                resolve(body);
        });
    });
}

function make_API_call_staging(sku, cookie) {
    const options = {
        method: 'GET',
        uri: planesEndpoint + sku.toLowerCase().replace(/[^0-9.]/g, "") + '/promotions/',
        json: true,
        headers: {
            Authorization: 'Basic ' + cookie
        }
    };
    return new Promise(resolve => {
        request(options, function(error, response, body) {
            var stringified = JSON.stringify(body);
            var parsed = JSON.parse(stringified);
            resolve(body);
        }, function(error, response, body) {
            if (!error)
                resolve(body);
        });
    });
};

module.exports = {
    make_API_call: make_API_call,
    make_API_call_staging: make_API_call_staging,
    validate_user_call: validate_user_call,
    make_API_call_MPromo: make_API_call_MPromo,
    make_API_call_productos: make_API_call_productos
}