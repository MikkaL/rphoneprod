var $jscomp = $jscomp || {};
$jscomp.scope = {};
$jscomp.arrayIteratorImpl = function(a) { var b = 0; return function() { return b < a.length ? { done: !1, value: a[b++] } : { done: !0 } } };
$jscomp.arrayIterator = function(a) { return { next: $jscomp.arrayIteratorImpl(a) } };
$jscomp.makeIterator = function(a) { var b = "undefined" != typeof Symbol && Symbol.iterator && a[Symbol.iterator]; return b ? b.call(a) : $jscomp.arrayIterator(a) };
$jscomp.arrayFromIterator = function(a) { for (var b, c = []; !(b = a.next()).done;) c.push(b.value); return c };
$jscomp.arrayFromIterable = function(a) { return a instanceof Array ? a : $jscomp.arrayFromIterator($jscomp.makeIterator(a)) };
var api = require("../api/index.js"),
    convert = require("xml-js"),
    xml2js = require("xml2js"),
    parseString = xml2js.parseString;

function look_up(a) { return new Promise(function(b, c) { api.make_API_call(a).then(function(a) { b(a) })["catch"](function(a) { c(a) }) }) }

function look_up_plan(a, b) { return new Promise(function(c, d) { api.make_API_call_staging(a, b).then(function(a) { c(a) })["catch"](function(a) { d(a) }) }) }

function look_up_prices(a, b) { return new Promise(function(c, d) { api.make_API_call_productos(a, b).then(function(b) { var f = createXml(b);
            api.make_API_call_MPromo(f).then(function(d) { c(parseResponse(d.substring(5, d.length), a, b.precioMaster, b.precioVigente)) })["catch"](function(a) { d(a) }) })["catch"](function(a) { d(a) }) }) }

function createXml(a) {
    try {
        var b = [];
        b.push({
            _attributes: {
                id: "1",
                cant: "1",
                upcimp: a.ean13,
                prod: a.ean13,
                descr: a.descripcion,
                division: a.division,
                area: a.area,
                depto: a.deptop,
                linea: a.linea,
                slinea: a.subLinea,
                evento: a.evento,
                prioridad: a.prioridad,
                sprioridad: a.subPrioridad,
                precio: a.precioVigente,
                precio1: a.precioVigente,
                estilo: a.ean13,
                marca_prod: a.marcaProd,
                marca_propia: a.marcaPropia,
                marca_prov: a.marcaPrv,
                prov_act: "00000000000",
                prov_ant: "00000000000",
                bdesc: "0",
                tipoart: a.tipoArticulo,
                proced: a.procedencia,
                rangoprecio: a.ranPrecio,
                tempo: a.temporada,
                concesion: a.concesion,
                fletedesp: a.fleteDbo,
                tapiceria: a.tapiceria,
                ciclovida: a.cicloVida,
                indus: a.industrializado,
                origdesp: a.oreDespachoBt,
                prodinternet: a.prdInternet,
                tiponego: a.tipNegociacion
            }
        });
        var c = {
            _declaration: { _attributes: { version: "1.0", encoding: "utf-8" } },
            pangui: {
                O: {
                    _attributes: { oper: "C" },
                    T: {
                        _attributes: { fecha: format(new Date), tienda: "12", pos: "Manu", ntrans: "99999999", tipo_trans: "1", comercio: "1", rpuntos: "0", total: "7990" },
                        C: { _attributes: { cliente: "rmisurut", tcliente: "", tevento: "" } },
                        PS: { P: { _attributes: { tpago: "1", moneda: "P", ncuotas: "0", tcuotas: "0", monto: "7990" } } },
                        LS: { L: [].concat($jscomp.arrayFromIterable(b)) }
                    }
                }
            }
        };
        return convert.json2xml(c, { compact: !0 })
    } catch (d) { return d }
}

function format(a) { var b = a.getMonth() + 1,
        c = a.getDate(),
        d = ("" + a.getFullYear()).substring(2, a.getFullYear().length),
        f = a.getHours();
    a = a.getMinutes(); return [(9 < c ? "" : "0") + c, (9 < b ? "" : "0") + b, d, (9 < f ? "" : "0") + f, (9 < a ? "" : "0") + a].join("") }

function parseResponse(a, b, c, d) {
    a = convert.xml2js(a).elements[0].elements;
    c = '{"precioMaster": ' + parseInt(c, 10) + ',"precioVigente": ' + parseInt(d, 10) + ',"promoPrices": [';
    for (i in a)
        for (e in c += '{"tr": "' + a[i].attributes.tr + '",', c += '"haypromo": "' + a[i].attributes.hay_promo + '",', a[i].elements) d = a[i].elements[e].elements[0].elements[0].attributes.prod.substring(1, a[i].elements[e].elements[0].elements[0].attributes.prod.length), d == b && (c += '"precio": ' + a[i].elements[e].elements[0].elements[0].attributes.precio +
            ",", c += '"prod": "' + d + '",', c += '"mdesc": ', mdesc = a[i].elements[e].elements[0].elements[0].attributes.mdesc, c += void 0 != mdesc ? mdesc + "}," : "0},");
    c = c.substring(0, c.length - 1) + "]}";
    return JSON.parse(c)
}
module.exports = { look_up: look_up, look_up_plan: look_up_plan, look_up_prices: look_up_prices };