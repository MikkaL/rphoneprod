const api = require('../api/index.js');

function log_in(user, password){
    return new Promise((resolve, reject) => {
        api.validate_user_call(user, password)
            .then((res) => {
                resolve(res);
            })
            .catch(error => {
                reject(error);
            })
    });
}

module.exports = {
	log_in: log_in
}