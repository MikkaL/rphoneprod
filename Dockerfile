FROM node:8
WORKDIR /rphone
COPY package.json .
RUN npm install
COPY . ./
EXPOSE 80
CMD [ “npm”, “start” ] 